package API;

import LZU.LoggerImplementation;

public class LoggerFactory {
    public static Logger createLogger () {
        return new LoggerImplementation();
    }
}
