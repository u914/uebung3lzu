package Exceptions;

public class MultipleStartMethodsException extends Exception{
    public MultipleStartMethodsException () {
        super("Es wurden mehrere Start Methoden gefunden.");
    }
}
