package Exceptions;

public class MultipleStopMethodsException extends Exception{
    public MultipleStopMethodsException() {
        super("Es wurden mehrere Stop Methoden gefunden.");
    }
}
