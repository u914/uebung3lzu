package State;

public interface ComponentState {
    void next();
    void previous();
    void printStatus();
}
