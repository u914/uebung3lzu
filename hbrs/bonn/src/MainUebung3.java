import Command.*;

import java.io.IOException;
import java.util.Scanner;

import Exceptions.MultipleStartMethodsException;
import Exceptions.MultipleStopMethodsException;
import LZU.*;
import LZU.ConfigurationManagement.LZUConfigurationManager;

public class MainUebung3 {

    public static void main(String[] args) throws MultipleStopMethodsException, MultipleStartMethodsException, IOException, ClassNotFoundException {
                
        /*
        Commands:
        LZU
            -startLZU --> Starten der LZU
            -stopLZU --> Stoppen der LZU

            #List of loaded Components
            -importComponent(url) --> Hinzufügen der Klassen in die alt.LZU // prüfen ob einmalig geladen.
            -removeComponent(id) --> Entfernen der Klassen aus der alt.LZU
            -listImports --> gibt Liste der geladenen Komponenten zurück

            -listComponents --> Listet alle Intanzen von Klassen // Status der Comp speichern in Liste.
            -createComponent --> Erstellt eine Instanz einer importierten Komponente
            -startComponent [list:id] --> Startet eine Instanz //Name evtl Name des Package oder selbst vergeben.
            -stopComponent [list:id] --> Beendet eine Instanz
        */

        /*LZU.getInstance().addComponent("file:hbrs/bonn/testJars/TestComponent.jar");
        LZU.getInstance().createComponent(0);
        LZU.getInstance().createComponent(0);
        LZU.getInstance().createComponent(0);
        LZU.getInstance().createComponent(0);
        System.out.println(LZU.getInstance().listImportedComponents());
        System.out.println(LZU.getInstance().listComponents());*/
        //LZUConfigurationManager.loadFromFile();
        //System.out.println(LZU.getInstance().listImportedComponents());
        //System.out.println(LZU.getInstance().listComponents());
        //LZU.getInstance().startComponent(0);
        //System.out.println(LZU.getInstance().listComponents());
        //LZU.getInstance().stopComponent(0);
        //System.out.println(LZU.getInstance().listComponents());
        startCLI();
    }
    
    private static void startCLI () {
        System.out.println("CLI gestartet!");
        Scanner scanner = new Scanner(System.in);
        LZUCommandExecuter commandExecuter = new LZUCommandExecuter();
        System.out.print("> ");
        
        while (scanner.hasNextLine()) {
            LZUOperation command = null;
            String line = scanner.nextLine();
            Scanner lineScanner = new Scanner(line);
            boolean skip = false;

            while (lineScanner.hasNext() && !skip) {
                switch (lineScanner.next().toLowerCase()) {
                    case "startlzu":
                        command = new StartLZUCommand();
                        break;
                    case "stoplzu":
                        command = new StopLZUCommand();
                        break;
                    case "importcomponent":
                        if (lineScanner.hasNext()) {
                            String url = "file:";
                            while (lineScanner.hasNext()) {
                                url += lineScanner.next();
                            }
                            command = new ImportComponentToLZUCommand(url);
                        } else {
                            System.out.println("Nutzung: importComponent [url]");
                        }
                        break;
                    case "removecomponent":
                        try {
                            command = new RemoveComponentFromLZUCommand(Integer.parseInt(lineScanner.next()));
                        } catch (NumberFormatException e) {
                            System.out.println("Der angegebene Parameter ist keine Zahl!");
                        }
                        break;
                    case "listimports":
                        command = new ListImportsCommand();
                        break;
                    case "listcomponents":
                        command = new ListComponentsCommand();
                        break;
                    case "createcomponent":
                        try {
                            command = new CreateComponentCommand(Integer.parseInt(lineScanner.next()));
                        } catch (NumberFormatException e) {
                            System.out.println("Der angegebene Parameter ist keine Zahl!");
                        }
                        break;
                    case "startcomponent":
                        try {
                            command = new StartComponentCommand(Integer.parseInt(lineScanner.next()));
                        } catch (NumberFormatException e) {
                            System.out.println("Der angegebene Parameter ist keine Zahl!");
                        }
                        break;
                    case "stopcomponent":
                        try {
                            command = new StopComponentCommand(Integer.parseInt(lineScanner.next()));
                        } catch (NumberFormatException e) {
                            System.out.println("Der angegebene Parameter ist keine Zahl!");
                        }
                        break;
                    case "state":
                        System.out.println(LZU.getInstance().getState(Integer.parseInt(lineScanner.next())));
                        break;
                    default:
                        System.out.println("Kommando nicht gefunden!");
                        skip = true;
                        break;
                }
            }

            if (command != null) {
                commandExecuter.executeCommand(command);
            }

            System.out.print("> ");
        }
    }

    public void recoverApplication () {
        try {
            LZUConfigurationManager.loadFromFile();
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Gespeicherter Stand konnte nicht eingelesen werden.");
        }
    }
}