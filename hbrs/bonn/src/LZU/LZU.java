package LZU;

import Exceptions.MultipleStartMethodsException;
import Exceptions.MultipleStopMethodsException;
import LZU.ConfigurationManagement.LZUConfigurationManager;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class LZU {
    private List<String> componentURLs;
    private Map<Component, String> components;
    private static LZU lzu;

    private LZU() {

    }

    public void stopLZU() {
        lzu = null;
        System.out.println("LZU gestoppt");
    }

    public static LZU getInstance() {
        if (lzu == null) {
            System.out.println("Starte LZU");
            lzu = new LZU();
            System.out.println("LZU gestartet");
        }

        return lzu;
    }

    public void addComponent(String url) throws IOException {
        // Prüfen, ob die URL korrekt geformt ist
        try {
            new URL(url);
        } catch (MalformedURLException e) {
            System.out.println("Der angegebene Parameter ist keine URL!");
            return;
        }

        if (componentURLs == null) {
            componentURLs = new ArrayList<>();
        }

        if (componentURLs.contains(url)) {
            System.out.println("Komponente bereits hinzugefügt");
            return;
        }

        url = url.substring(5);

        componentURLs.add(url);
        System.out.println("Komponente hinzugefügt");
        LZUConfigurationManager.saveState(componentURLs,components);
    }

    public void removeComponent(int id) throws IOException {
        try {
            componentURLs.remove(id);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Die angegebene ID ist nicht in der Liste enthalten!");
        } catch (NullPointerException e) {
            System.out.println("Es wurden noch keine Komponenten hinzugefügt.");
        }
        LZUConfigurationManager.saveState(componentURLs,components);
    }

    public HashMap<Integer, String> listImportedComponents() {
        HashMap<Integer, String> output = new HashMap<>();
        int i = 0;

        for (String c : componentURLs) {
            output.put(i, c);
        }

        return output;
    }

    public void createComponent(int id) throws IOException {
        String url;
        try {
            url = componentURLs.get(id);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("ID nicht in der Liste der Komponenten.");
            return;
        }

        if (components == null) {
            components = new LinkedHashMap<>();
        }

        try {
            Component newComponent = new Component(url);
            components.put(newComponent, url);
        } catch (MalformedURLException | ClassNotFoundException | MultipleStartMethodsException | MultipleStopMethodsException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("URL wurde nicht gefunden!");
            e.printStackTrace();
        }
        LZUConfigurationManager.saveState(componentURLs,components);
    }

    public void startComponent (int componentID) throws IOException {
        try {
            getComponentByIndex(componentID).start();
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Die angegebene ID ist nicht in der Liste vorhanden.");
        } catch (NullPointerException e) {
            System.out.println("Es wurden noch keine Komponenten erstellt.");
        }
        LZUConfigurationManager.saveState(componentURLs,components);
    }

    public void stopComponent (int componentID) throws IOException {
        try {
            getComponentByIndex(componentID).stopInit();
            getComponentByIndex(componentID).interrupt();
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Die angegebene ID ist nicht in der Liste vorhanden.");
        } catch (NullPointerException e) {
            System.out.println("Es wurden noch keine Komponenten erstellt.");
        }
        LZUConfigurationManager.saveState(componentURLs,components);
    }


    public String listComponents() {
        StringBuilder sb = new StringBuilder();

        if (components == null) {
            System.out.println("Es wurden noch keine Komponenten erstellt!");
            return "";
        }

        int id = 0;
        sb.append("ID  |   Name    |   Status  |   URL");
        sb.append("\n");
        for (Map.Entry<Component, String> e: components.entrySet()) {
            String idString = Integer.toString(id);
            String name = e.getKey().getName();
            String status = e.getKey().getStatus();
            String url = e.getValue();
            sb.append(idString).append("   |   ").append(name).append("    |   ").append(status).append("    |   ").append(url);
            sb.append("\n");
            ++id;
        }

        return sb.toString();
    }

    private Component getComponentByIndex (int index) {
        return (Component) components.keySet().toArray()[index];
    }

    public String getState(int id) {
        return String.valueOf(getComponentByIndex(id).getStatus());
    }

    public Map<Component, String> getComponents() {
        return components;
    }
}
