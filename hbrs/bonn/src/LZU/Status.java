package LZU;

public enum Status {
    STARTED ("Started"),
    STOPPED ("Stopped"),
    CREATED ("Created");

    private String status;

    Status (String status) {
        this.status = status;
    }

    String getStatus() {
        return status;
    }
}
