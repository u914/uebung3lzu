package LZU.ConfigurationManagement;

import java.io.Serializable;

public class WriteObject implements Serializable {
    private String url;
    private String status;
    private static int id;

    public WriteObject (String url, String status) {
        this.url = url;
        this.status = status;
        id++;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static int getId() {
        return id;
    }

    public static void setId(Integer id) {
        WriteObject.id = id;
    }

    @Override
    public String toString() {
        return "WriteObject{" +
                "url='" + url + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
