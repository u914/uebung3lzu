package LZU.ConfigurationManagement;

import LZU.Component;
import LZU.LZU;

import java.io.*;
import java.util.*;

public class LZUConfigurationManager {

    public static void saveState (List<String> urls, Map<Component, String> components) throws IOException {
        List<WriteObject> writeObjects = new ArrayList<>();

        if (components != null) {
            for (Map.Entry<Component, String> entry : components.entrySet()) {
                writeObjects.add(new WriteObject(entry.getValue(), entry.getKey().getStatus()));
            }
        } else {
            writeObjects.add(null);
        }

        LinkedHashMap<List<String>, List<WriteObject>> data = new LinkedHashMap<>();
        data.put(urls, writeObjects);

        File file = new File("TESTFILE");
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(data);
    }

    public static void loadFromFile () throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream("TESTFILE");
        ObjectInputStream ois = new ObjectInputStream(fis);
        LinkedHashMap<List<String>, List<WriteObject>> woi = (LinkedHashMap<List<String>, List<WriteObject>>) ois.readObject();
        List<WriteObject> writeObjects = (List<WriteObject>) woi.values().toArray()[0];
        List<String> urlList = (List<String>) woi.keySet().toArray()[0];

        for (String url : urlList) {
            LZU.getInstance().addComponent("file:"+url);
        }

        for (WriteObject w : writeObjects) {
            switch (w.getStatus().toLowerCase()) {
                case "created" -> LZU.getInstance().createComponent(urlList.indexOf(w.getUrl()));
                case "started" -> {
                    LZU.getInstance().createComponent(urlList.indexOf(w.getUrl()));
                    LZU.getInstance().startComponent(LZU.getInstance().getComponents().size());
                }
                case "stopped" -> {
                    LZU.getInstance().createComponent(urlList.indexOf(w.getUrl()));
                    LZU.getInstance().startComponent(LZU.getInstance().getComponents().size());
                    LZU.getInstance().stopComponent(LZU.getInstance().getComponents().size());
                }
            }
        }
    }
}
