package LZU;

import API.Inject;
import API.Logger;
import API.Start;
import API.Stop;
import Exceptions.MultipleStartMethodsException;
import Exceptions.MultipleStopMethodsException;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class Component extends Thread {

    private ClassLoader classLoader;
    private Class<?> startClass;
    private Method startMethod;
    private Method stopMethod;
    private Map<String, Class<?>> classes;
    private Object componentInstance;
    private Status status;
    private String url;

    public Component() {
    }

    public Component (String url) throws IOException, ClassNotFoundException, MultipleStartMethodsException, MultipleStopMethodsException {
        setClassLoader(url);
        loadClassFromJAR(url);
        findStartMethod();
        findStopMethod();
        this.url = url;
        status = Status.CREATED;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public Class<?> getStartClass() {
        return startClass;
    }

    public Method getStartMethod() {
        return startMethod;
    }

    public Method getStopMethod() {
        return stopMethod;
    }

    public Map<String, Class<?>> getClasses() {
        return classes;
    }

    public void setClassLoader (String url) throws MalformedURLException {
        URL[] urls = { new URL("jar:file:" + url +"!/") };
        this.classLoader = URLClassLoader.newInstance(urls);
    }

    public void loadClassFromJAR (String url) throws ClassNotFoundException, IOException, NullPointerException {
        JarFile jarFile;
        Enumeration<JarEntry> jarEntries;

        jarFile = new JarFile(url);
        jarEntries = jarFile.entries();

        this.classes = new HashMap<>();

        while (jarEntries.hasMoreElements()) {
            JarEntry je = jarEntries.nextElement();

            if(je.isDirectory() || !je.getName().endsWith(".class")){
                continue;
            }
            // -6 because of .class
            String className = je.getName().substring(0,je.getName().length()-6);
            className = className.replace('/', '.');

            if(classes.containsKey(className)) {
                System.out.println("Klasse bereits geladen!");
                continue;
            }

            //TODO Hier noch Try/Catch!
            Class<?> c = classLoader.loadClass(className);

            classes.put(className, c);
            System.out.println("Klasse geladen: " + className);
        }
    }

    public void findStartMethod () throws MultipleStartMethodsException {
        final Class<? extends Annotation> startAnnotation = Start.class;

        for (Class<?> clazz : classes.values()) {
            for (Method method : clazz.getDeclaredMethods()) {
                if (method.isAnnotationPresent(startAnnotation)) {
                    if (startMethod != null) {
                        throw new MultipleStartMethodsException();
                    }
                    startClass = clazz;
                    startMethod = method;
                }
            }
        }

        System.out.println("Start Methode gefunden mit dem Namen: " + startMethod.getName());
    }

    public void findStopMethod () throws  MultipleStopMethodsException {
        final Class<? extends Annotation> stopAnnotation = Stop.class;

        for (Class<?> clazz : classes.values()) {
            for (Method method : clazz.getDeclaredMethods()) {
                if (method.isAnnotationPresent(stopAnnotation)) {
                    if (stopMethod != null) {
                        throw new MultipleStopMethodsException();
                    }
                    stopMethod = method;
                }
            }
        }

        System.out.println("Stop Methode gefunden mit dem Namen: " + stopMethod.getName());
    }

    public void injectLogger (Logger logger) throws IllegalAccessException {
        final Class<? extends Annotation> injectionAnnotation = Inject.class;

        for (Class<?> clazz : classes.values()) {
            for (Field field : clazz.getDeclaredFields()) {
                if (field.isAnnotationPresent(injectionAnnotation)) {
                    if (field.getGenericType() == Logger.class) {
                        field.setAccessible(true);
                        field.set(this.componentInstance, logger);
                    }
                }
            }
        }
    }

    public void startInit () {
        try {
            this.injectLogger(new LoggerImplementation());
            startMethod.invoke(componentInstance);
            this.status = Status.STARTED;
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void stopInit () {
        try {
            stopMethod.invoke(componentInstance);
            this.status = Status.STOPPED;
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            componentInstance = startClass.getConstructor().newInstance();
            startInit();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public String getStatus() {
        return status.getStatus();
    }

    public String getUrl() {
        return url;
    }
}
