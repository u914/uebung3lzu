package LZU;

import API.Logger;

import java.sql.Timestamp;
import java.time.Instant;

public class LoggerImplementation implements Logger {

    private static Logger logger;

    public LoggerImplementation () {

    }

    public static Logger getInstance() {
        if (logger == null) {
            logger = new LoggerImplementation();
        }
        return logger;
    }


    @Override
    public void sendLog(String s) {
        System.out.println("++++ LOG: Meldung aus Component: " + s + "(" + Timestamp.from(Instant.now()) + ")");
    }
}
