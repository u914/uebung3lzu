package Command;

import LZU.LZU;

import java.io.IOException;
import java.net.MalformedURLException;

public class ImportComponentToLZUCommand implements LZUOperation{

    private String url;

    public ImportComponentToLZUCommand(String url) {
        this.url = url;
    }

    @Override
    public void execute() {
        try {
            LZU.getInstance().addComponent(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
