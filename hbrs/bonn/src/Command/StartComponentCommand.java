package Command;

import java.io.IOException;

public class StartComponentCommand implements LZUOperation {

    private int id;

    public StartComponentCommand (int id) {
        this.id = id;
    }

    @Override
    public void execute() {
        try {
            LZU.LZU.getInstance().startComponent(this.id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
