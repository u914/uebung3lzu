package Command;

import java.io.IOException;

public class CreateComponentCommand implements LZUOperation {

    private int id;

    public CreateComponentCommand (int id) {
        this.id = id;
    }

    @Override
    public void execute() {
        try {
            LZU.LZU.getInstance().createComponent(this.id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
