package Command;

import LZU.*;

public class ListImportsCommand implements LZUOperation {
    @Override
    public void execute() {
        System.out.println(LZU.getInstance().listImportedComponents().toString());
    }
}
