package Command;

import LZU.LZU;

public class StopLZUCommand implements LZUOperation{
    @Override
    public void execute() {
        LZU.getInstance().stopLZU();
    }
}
