package Command;

import java.io.IOException;

public class StopComponentCommand implements LZUOperation {

    private int id;

    public StopComponentCommand (int id) {
        this.id = id;
    }

    @Override
    public void execute() {
        try {
            LZU.LZU.getInstance().stopComponent(id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
