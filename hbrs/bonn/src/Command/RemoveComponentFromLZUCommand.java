package Command;

import LZU.LZU;

import java.io.IOException;

public class RemoveComponentFromLZUCommand implements LZUOperation{

    private int id;

    public RemoveComponentFromLZUCommand (int id) {
        this.id = id;
    }


    @Override
    public void execute() {
        try {
            LZU.getInstance().removeComponent(id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
