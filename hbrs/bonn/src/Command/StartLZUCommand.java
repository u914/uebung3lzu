package Command;

import LZU.*;

public class StartLZUCommand implements LZUOperation{
    @Override
    public void execute() {
        LZU.getInstance();
    }
}
