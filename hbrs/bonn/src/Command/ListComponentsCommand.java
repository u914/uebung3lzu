package Command;

import LZU.LZU;

import java.util.HashMap;

public class ListComponentsCommand implements LZUOperation{
    @Override
    public void execute() {
        System.out.println(LZU.getInstance().listComponents().toString());
    }
}
