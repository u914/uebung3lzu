package Command;

import java.util.ArrayList;
import java.util.List;

public class LZUCommandExecuter {
    private List<LZUOperation> operations = new ArrayList<>();

    public void executeCommand (LZUOperation operation) {
        operation.execute();
        operations.add(operation);
    }
}
